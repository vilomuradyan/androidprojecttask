package org.vilhelm.muradyan.androidtest.features.details

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController
import org.koin.android.viewmodel.ext.android.viewModel
import org.vilhelm.muradyan.androidtest.R
import org.vilhelm.muradyan.androidtest.databinding.FragmentDetailsBinding
import org.vilhelm.muradyan.androidtest.viewModels.DetailsViewModel
import org.vilhelm.muradyan.core.AppConstants
import org.vilhelm.muradyan.core.base.BaseFragment
import org.vilhelm.muradyan.core.base.BaseViewModel
import org.vilhelm.muradyan.domain.model.ContactResultItem

class DetailsFragment : BaseFragment<FragmentDetailsBinding>(R.layout.fragment_details),
    View.OnClickListener {

    private val mViewModel by viewModel<DetailsViewModel>()

    override fun getViewModel(): BaseViewModel = mViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mBinding.data = requireArguments()[AppConstants.CONTACT] as ContactResultItem
        initListener()
    }

    private fun initListener() {
        mBinding.actionBack.setOnClickListener(this)
        mBinding.labelPhone.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            mBinding.actionBack -> findNavController().popBackStack()
            mBinding.labelPhone -> checkPermission()
        }
    }

    private fun navigateToTell() {
        startActivity(Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:${mBinding.labelPhone.text}")))
    }

    private fun checkPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CALL_PHONE
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            navigateToTell()
        } else {
            requestPermissions(
                Array(1) { android.Manifest.permission.CALL_PHONE }, 1
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            0 -> {
                if (checkPermission(grantResults)) {
                    navigateToTell()
                }
            }
        }
    }

    private fun checkPermission(grantResults: IntArray) =
        grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
}