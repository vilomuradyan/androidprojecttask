package org.vilhelm.muradyan.androidtest

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.vilhelm.muradyan.androidtest.di.*
import org.vilhelm.muradyan.local.dataBaseModule

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(
                dataBaseModule,
                networkModule,
                viewModelModules,
                localDataSourceModule,
                remoteDataModule,
                useCaseModule,
            )
        }
    }
}