package org.vilhelm.muradyan.androidtest.features.contact

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.core.os.bundleOf
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel
import org.vilhelm.muradyan.androidtest.R
import org.vilhelm.muradyan.androidtest.databinding.FragmentContactBinding
import org.vilhelm.muradyan.androidtest.features.contact.adapters.ContactAdapter
import org.vilhelm.muradyan.androidtest.viewModels.ContactViewModel
import org.vilhelm.muradyan.core.AppConstants
import org.vilhelm.muradyan.core.base.BaseFragment
import org.vilhelm.muradyan.core.base.BaseViewModel
import org.vilhelm.muradyan.core.remove
import org.vilhelm.muradyan.core.show
import org.vilhelm.muradyan.domain.model.ContactResultItem

class ContactFragment : BaseFragment<FragmentContactBinding>(R.layout.fragment_contact) {

    private val mViewModel by viewModel<ContactViewModel>()
    private var currentItems: List<ContactResultItem> = mutableListOf()
    private var contactAdapter: ContactAdapter? = null

    override fun getViewModel(): BaseViewModel = mViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initRecycler()
        handleError()
        searchContact()
        initListeners()
        initSearch()
    }

    private fun initListeners() {
        mBinding.actionClose.setOnClickListener {
            contactAdapter?.updateData(currentItems)
            mBinding.inputSearch.apply {
                text.clear()
                clearFocus()
            }
            mBinding.actionClose.remove()
        }
        mBinding.swipeRefreshLayout.setOnRefreshListener {
            mViewModel.getDataFromServer()
        }
    }

    private fun initRecycler() {
        lifecycleScope.launch {
            mViewModel.contact.collect {
                mBinding.swipeRefreshLayout.isRefreshing = false
                if (it.isNotEmpty()) {
                    currentItems = it
                    contactAdapter = ContactAdapter { items ->
                        findNavController().navigate(
                            R.id.detailsFragment,
                            bundleOf(Pair(AppConstants.CONTACT, items))
                        )
                    }.apply {
                        updateData(it)
                    }
                    mBinding.progressBar.remove()
                    mBinding.contactList.apply {
                        setHasFixedSize(true)
                        adapter = contactAdapter
                    }
                }
                mViewModel.changeContactData()
            }
        }
    }

    private fun handleError() {
        lifecycleScope.launch {
            mViewModel.error.collect {
                it?.let {
                    mBinding.progressBar.remove()
                    mViewModel.showErrorMessage(it.message)
                    mViewModel.changeErrorMessage()
                }
            }
        }
    }

    private fun searchContact() {
        mBinding.inputSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mBinding.actionClose.show()
            }

            override fun afterTextChanged(s: Editable?) {
                filterData(s)
            }
        })
    }

    private fun filterData(s: Editable?) {
        if (s.toString().isNotEmpty()) {
            mViewModel.getSearchData("%$s%")
        }
    }

    private fun initSearch() {
        lifecycleScope.launch {
            mViewModel.search.collect {
                val data = mutableListOf<ContactResultItem>()
                it.forEach { newData ->
                    data.add(
                        ContactResultItem(
                            id = newData.id,
                            biography = newData.biography,
                            educationPeriod = newData.educationPeriod,
                            height = newData.height,
                            name = newData.name,
                            phone = newData.phone,
                            temperament = newData.temperament
                        )
                    )
                }
                contactAdapter?.updateData(data)
            }
        }
    }
}