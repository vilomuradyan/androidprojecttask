package org.vilhelm.muradyan.androidtest.di

import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.vilhelm.muradyan.domain.repository.IContactRepository
import org.vilhelm.muradyan.remote.repository.FirstContactRepository
import org.vilhelm.muradyan.remote.repository.SecondContactRepository
import org.vilhelm.muradyan.remote.repository.ThirdContactRepository

val remoteDataModule = module {
    single<IContactRepository>(named("first")) {
        FirstContactRepository(
            apiService = get(),
            contactDao = get()
        )
    }
    single<IContactRepository>(named("second")) {
        SecondContactRepository(
            apiService = get(),
            contactDao = get()
        )
    }
    single<IContactRepository>(named("third")) {
        ThirdContactRepository(
            apiService = get(),
            contactDao = get()
        )
    }
}

