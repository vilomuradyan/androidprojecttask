package org.vilhelm.muradyan.androidtest.di

import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.vilhelm.muradyan.androidtest.viewModels.ContactViewModel
import org.vilhelm.muradyan.androidtest.viewModels.DetailsViewModel

val viewModelModules = module {
    viewModel {
        ContactViewModel(
            contactBaseUseCase = get(named("contact_first")),
            secondContactBaseUseCase = get(named("contact_second")),
            thirdContactBaseUseCase = get(named("contact_third")),
            contactDao = get(),
            sharedPreferences = get(named("settingsPrefs"))
        )
    }
    viewModel { DetailsViewModel() }
}