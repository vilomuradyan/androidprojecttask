package org.vilhelm.muradyan.androidtest.di

import org.koin.core.qualifier.named
import org.koin.dsl.module
import org.vilhelm.muradyan.domain.repository.IContactRepository
import org.vilhelm.muradyan.domain.useCases.*

val useCaseModule = module {
    single(named("contact_first")) { provideFirstContactUseCase(get(named("first"))) }
    single(named("contact_second")) { provideSecondContactUseCase(get(named("second"))) }
    single(named("contact_third")) { provideThirdContactUseCase(get(named("third"))) }
}

fun provideFirstContactUseCase(iContactRepository: IContactRepository): FirstContactBaseUseCase {
    return FirstContactUseCase(iContactRepository)
}

fun provideSecondContactUseCase(iContactRepository: IContactRepository): SecondContactBaseUseCase {
    return SecondContactUseCase(iContactRepository)
}

fun provideThirdContactUseCase(iContactRepository: IContactRepository): ThirdContactBaseUseCase {
    return ThirdContactUseCase(iContactRepository)
}

