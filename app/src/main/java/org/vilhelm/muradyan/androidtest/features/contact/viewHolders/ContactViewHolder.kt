package org.vilhelm.muradyan.androidtest.features.contact.viewHolders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.vilhelm.muradyan.androidtest.databinding.ItemContactsBinding
import org.vilhelm.muradyan.domain.model.ContactResultItem

class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mBinding = ItemContactsBinding.bind(itemView.rootView)

    fun bind(data: ContactResultItem) {
        mBinding.contact = data
    }
}