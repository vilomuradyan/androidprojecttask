package org.vilhelm.muradyan.androidtest.viewModels

import android.content.SharedPreferences
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.zip
import kotlinx.coroutines.launch
import org.vilhelm.muradyan.core.Event
import org.vilhelm.muradyan.core.base.BaseViewModel
import org.vilhelm.muradyan.core.model.ErrorResponse
import org.vilhelm.muradyan.core.utils.PreferencesUtils
import org.vilhelm.muradyan.domain.model.ContactResultItem
import org.vilhelm.muradyan.domain.useCases.FirstContactBaseUseCase
import org.vilhelm.muradyan.domain.useCases.SecondContactBaseUseCase
import org.vilhelm.muradyan.domain.useCases.ThirdContactBaseUseCase
import org.vilhelm.muradyan.local.dao.ContactDao
import org.vilhelm.muradyan.local.entity.ContactEntity
import java.util.*

class ContactViewModel(
    private val contactBaseUseCase: FirstContactBaseUseCase,
    private val secondContactBaseUseCase: SecondContactBaseUseCase,
    private val thirdContactBaseUseCase: ThirdContactBaseUseCase,
    private val contactDao: ContactDao,
    private val sharedPreferences: SharedPreferences
) : BaseViewModel() {

    private val _contact: MutableStateFlow<List<ContactResultItem>> =
        MutableStateFlow(mutableListOf())
    val contact: MutableStateFlow<List<ContactResultItem>> get() = _contact

    private val _error: MutableStateFlow<ErrorResponse?> = MutableStateFlow(null)
    val error: MutableStateFlow<ErrorResponse?> get() = _error

    private val _search: MutableStateFlow<List<ContactEntity>> = MutableStateFlow(mutableListOf())
    val search: MutableStateFlow<List<ContactEntity>> get() = _search

    init {
        getData()
    }

    fun getSearchData(search: String) = viewModelScope.launch(Dispatchers.Default) {
        _search.value = contactDao.getSearchData(search)
    }

    fun changeErrorMessage() {
        _error.value = null
    }

    fun changeContactData() {
        _contact.value = mutableListOf()
    }

    private fun getData() = viewModelScope.launch {
        val time = PreferencesUtils.getTime(sharedPreferences)
        if (time != 0L && Calendar.getInstance().timeInMillis.minus(time) < 60000L) getFromDb()
        else getDataFromServer()
    }

    fun showErrorMessage(message: String) {
        _snackBarError.value = Event(message)
    }

    private fun getFromDb() = viewModelScope.launch {
        val data = mutableListOf<ContactResultItem>()
        contactDao.getAll().forEach {
            data.add(
                ContactResultItem(
                    id = it.id,
                    biography = it.biography,
                    educationPeriod = it.educationPeriod,
                    height = it.height,
                    name = it.name,
                    phone = it.phone,
                    temperament = it.temperament
                )
            )
        }
        _contact.value = data
    }

    fun getDataFromServer() = viewModelScope.launch {
        val result = mutableListOf<ContactResultItem>()
        contactBaseUseCase.invoke(Unit)
            .zip(secondContactBaseUseCase.invoke(Unit)) { first, second ->
                result.addAll(first)
                result.addAll(second)
            }.zip(thirdContactBaseUseCase.invoke(Unit)) { _, third ->
                result.addAll(third)
            }.catch {
                _error.value = errorHandle(it)
            }.collect {
                PreferencesUtils.saveTime(sharedPreferences, Calendar.getInstance().timeInMillis)
                _contact.value = result
            }
    }
}