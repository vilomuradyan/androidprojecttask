package org.vilhelm.muradyan.androidtest.features.contact.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import org.vilhelm.muradyan.androidtest.R
import org.vilhelm.muradyan.androidtest.features.contact.ContactItemDiffCallback
import org.vilhelm.muradyan.androidtest.features.contact.viewHolders.ContactViewHolder
import org.vilhelm.muradyan.domain.model.ContactResultItem

class ContactAdapter(private val callback: (items: ContactResultItem) -> Unit) :
    RecyclerView.Adapter<ContactViewHolder>() {

    private val mItems = mutableListOf<ContactResultItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder =
        ContactViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_contacts, parent, false)
        )

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(mItems[position])
        holder.itemView.setOnClickListener { callback.invoke(mItems[position]) }
    }

    override fun getItemCount(): Int = mItems.size

    fun updateData(items: List<ContactResultItem>) {
        val diffCallback = ContactItemDiffCallback(mItems, items)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        mItems.clear()
        mItems.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }
}