package org.vilhelm.muradyan.androidtest.features.contact

import androidx.recyclerview.widget.DiffUtil
import org.vilhelm.muradyan.domain.model.ContactResultItem

class ContactItemDiffCallback(
    private val oldList: List<ContactResultItem>,
    private val newList: List<ContactResultItem>
) : DiffUtil.Callback() {

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
                && oldList[oldItemPosition].biography == newList[newItemPosition].biography
                && oldList[oldItemPosition].educationPeriod == newList[newItemPosition].educationPeriod
                && oldList[oldItemPosition].phone == newList[newItemPosition].phone
                && oldList[oldItemPosition].height == newList[newItemPosition].height
                && oldList[oldItemPosition].temperament == newList[newItemPosition].temperament
                && oldList[oldItemPosition].name == newList[newItemPosition].name
    }
}