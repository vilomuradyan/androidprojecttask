package org.vilhelm.muradyan.domain.useCases

import kotlinx.coroutines.flow.Flow
import org.vilhelm.muradyan.domain.model.ContactResultItem
import org.vilhelm.muradyan.domain.repository.IContactRepository

typealias FirstContactBaseUseCase = BaseUseCase<Unit, Flow<List<ContactResultItem>>>

class FirstContactUseCase(
    private val contactRepository: IContactRepository
) : FirstContactBaseUseCase {
    override suspend fun invoke(params: Unit): Flow<List<ContactResultItem>> =
        contactRepository.getContact()
}