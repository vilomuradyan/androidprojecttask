package org.vilhelm.muradyan.domain.model

import java.io.Serializable

data class ContactResultItem(
    val biography: String,
    val educationPeriod: EducationPeriod,
    val height: Double,
    val id: String,
    val name: String,
    val phone: String,
    val temperament: String
) : Serializable