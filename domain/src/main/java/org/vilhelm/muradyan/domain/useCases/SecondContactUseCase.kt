package org.vilhelm.muradyan.domain.useCases

import kotlinx.coroutines.flow.Flow
import org.vilhelm.muradyan.domain.model.ContactResultItem
import org.vilhelm.muradyan.domain.repository.IContactRepository

typealias SecondContactBaseUseCase = BaseUseCase<Unit, Flow<List<ContactResultItem>>>

class SecondContactUseCase(
    private val iContactRepository: IContactRepository
) : SecondContactBaseUseCase {
    override suspend fun invoke(params: Unit): Flow<List<ContactResultItem>> =
        iContactRepository.getContact()
}