package org.vilhelm.muradyan.domain.repository

import kotlinx.coroutines.flow.Flow
import org.vilhelm.muradyan.domain.model.ContactResultItem

interface IContactRepository {
    suspend fun getContact() : Flow<List<ContactResultItem>>
}