package org.vilhelm.muradyan.domain.model

data class EducationPeriod(
    val end: String,
    val start: String
)