package org.vilhelm.muradyan.domain.useCases

import kotlinx.coroutines.flow.Flow
import org.vilhelm.muradyan.domain.model.ContactResultItem
import org.vilhelm.muradyan.domain.repository.IContactRepository

typealias ThirdContactBaseUseCase = BaseUseCase<Unit, Flow<List<ContactResultItem>>>

class ThirdContactUseCase(
    private val iContactRepository: IContactRepository
) : ThirdContactBaseUseCase {
    override suspend fun invoke(params: Unit): Flow<List<ContactResultItem>> =
        iContactRepository.getContact()
}