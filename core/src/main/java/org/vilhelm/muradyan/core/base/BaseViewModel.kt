package org.vilhelm.muradyan.core.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDirections
import com.google.gson.Gson
import org.vilhelm.muradyan.core.Event
import org.vilhelm.muradyan.core.NavigationCommand
import org.vilhelm.muradyan.core.model.ErrorResponse
import retrofit2.HttpException

abstract class BaseViewModel : ViewModel() {

    // FOR ERROR HANDLER
    protected val _snackBarError = MutableLiveData<Event<Any>>()
    val snackBarError: LiveData<Event<Any>> get() = _snackBarError

    // FOR NAVIGATION
    private val _navigation = MutableLiveData<Event<NavigationCommand>>()
    val navigation: LiveData<Event<NavigationCommand>> = _navigation

    fun navigate(directions: NavDirections) {
        _navigation.value = Event(NavigationCommand.To(directions))
    }

    protected fun errorHandle(throwable: Throwable): ErrorResponse? {
        return try {
            if (throwable is HttpException) {
                throwable.response()!!.errorBody()!!.string()?.let {
                    Gson().fromJson(it, ErrorResponse::class.java)
                }
            } else {
                throwable.message?.let {
                    ErrorResponse(it)
                }
            }
        } catch (exception: Exception) {
            ErrorResponse("Something went wrong!")
        }
    }
}