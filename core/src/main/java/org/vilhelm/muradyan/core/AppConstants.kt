package org.vilhelm.muradyan.core

object AppConstants {

    const val BASE_URL =
        "https://raw.githubusercontent.com/SkbkonturMobile/mobile-test-droid/master/json/"
    const val BASE = "base"
    const val TIMEOUT = 60L
    const val CONTACT = "contact"
    const val FIRST_RUN = "first_run"
    const val TIME = "time"
}