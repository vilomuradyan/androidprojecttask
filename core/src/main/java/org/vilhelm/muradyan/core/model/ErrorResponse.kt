package org.vilhelm.muradyan.core.model

class ErrorResponse(
    val name: String,
    val message: String,
    val code: Int,
    val status: Int,
    val type: String
) {
    constructor(message: String):this("",message, 0, 0,"")
}