package org.vilhelm.muradyan.core.utils

import android.content.SharedPreferences
import org.vilhelm.muradyan.core.AppConstants

object PreferencesUtils {

    fun saveTime(sharedPreferences: SharedPreferences, time: Long) =
        sharedPreferences.edit().putLong(AppConstants.TIME, time).apply()

    fun getTime(sharedPreferences: SharedPreferences) =
        sharedPreferences.getLong(AppConstants.TIME, 0)

}