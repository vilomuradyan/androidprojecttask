package org.vilhelm.muradyan.local

import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val dataBaseModule = module {
    single(named("database")) { ArchAppDatabase.buildDatabase(androidContext()) }
    factory { (get(named("database")) as ArchAppDatabase).contactDao() }
}