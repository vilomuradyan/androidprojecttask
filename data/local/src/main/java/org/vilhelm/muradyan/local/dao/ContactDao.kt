package org.vilhelm.muradyan.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import org.vilhelm.muradyan.local.entity.ContactEntity

@Dao
abstract class ContactDao {

    @Query("SELECT * FROM contactentity")
    abstract suspend fun getAll(): List<ContactEntity>

    @Insert
    abstract suspend fun insertAll(users: List<ContactEntity>)

    @Query("SELECT * FROM contactentity WHERE name LIKE :search")
    abstract fun getSearchData(search: String): List<ContactEntity>

    @Query("DELETE FROM contactentity")
    abstract suspend fun clearDb()
}