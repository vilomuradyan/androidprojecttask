package org.vilhelm.muradyan.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.vilhelm.muradyan.domain.model.EducationPeriod


@Entity
data class ContactEntity(
    @PrimaryKey val id: String,
    val biography: String,
    val educationPeriod: EducationPeriod,
    val height: Double,
    val name: String,
    val phone: String,
    val temperament: String
)