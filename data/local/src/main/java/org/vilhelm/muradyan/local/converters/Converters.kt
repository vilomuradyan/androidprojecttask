package org.vilhelm.muradyan.local.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import org.vilhelm.muradyan.domain.model.EducationPeriod

class Converters {

    @TypeConverter
    fun fromEducationPeriod(educationPeriod: EducationPeriod): String =
        Gson().toJson(educationPeriod)

    @TypeConverter
    fun toEducationPeriod(start: String): EducationPeriod =
        Gson().fromJson(start, EducationPeriod::class.java)
}