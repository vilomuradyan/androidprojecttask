package org.vilhelm.muradyan.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.vilhelm.muradyan.local.converters.Converters
import org.vilhelm.muradyan.local.dao.ContactDao
import org.vilhelm.muradyan.local.entity.ContactEntity

@Database(entities = [ContactEntity::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class ArchAppDatabase : RoomDatabase() {

    // DAO
    abstract fun contactDao(): ContactDao

    companion object {
        fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                ArchAppDatabase::class.java,
                "ArchApp.db"
            ).build()
    }
}