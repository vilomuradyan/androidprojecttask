package org.vilhelm.muradyan.remote.api

import org.vilhelm.muradyan.domain.model.ContactResult
import retrofit2.http.GET

interface ApiService {

    @GET("generated-01.json")
    suspend fun getFirstContacts(): ContactResult

    @GET("generated-02.json")
    suspend fun getSecondContacts(): ContactResult

    @GET("generated-03.json")
    suspend fun getThirdContacts(): ContactResult
}