package org.vilhelm.muradyan.remote.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.vilhelm.muradyan.domain.model.ContactResultItem
import org.vilhelm.muradyan.domain.repository.IContactRepository
import org.vilhelm.muradyan.local.dao.ContactDao
import org.vilhelm.muradyan.local.entity.ContactEntity
import org.vilhelm.muradyan.remote.api.ApiService

class ThirdContactRepository(
    private val apiService: ApiService,
    private val contactDao: ContactDao
) : IContactRepository {
    override suspend fun getContact(): Flow<List<ContactResultItem>> = flow {
        val data = apiService.getThirdContacts()
        val dataForDb = mutableListOf<ContactEntity>()
        data.forEach {
            dataForDb.add(
                ContactEntity(
                    id = it.id,
                    biography = it.biography,
                    educationPeriod = it.educationPeriod,
                    height = it.height,
                    name = it.name,
                    phone = it.phone,
                    temperament = it.temperament
                )
            )
        }
        contactDao.insertAll(dataForDb)
        emit(data)
    }.flowOn(Dispatchers.IO)
}